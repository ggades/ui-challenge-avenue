import React, { PropTypes } from 'react';
import _ from 'lodash';
import Modal from 'react-bootstrap/lib/Modal';

import './style.less';

class Error extends React.Component {

  constructor(props) {
    super(props);

    // Bind
    this.hideModal = this.hideModal.bind(this);
  }

  hideModal() {
    this.props.onDismissErrors();
  }

  render() {
    let showModal = false;
    const modalBody = [];
    if (this.props.errors && this.props.errors.length) {
      showModal = true;
      _.forEach(this.props.errors, (msg, i) => {
        let errorMessage = null;
        if (msg.data) {
          errorMessage = `${msg.data}`;
        } else {
          errorMessage = msg;
        }

        modalBody.push(<p key={i}>{errorMessage}</p>);
      });
    }

    return (
      <div className="modal-error">
        <Modal
          show={showModal}
          onHide={this.hideModal}
          bsSize="small">
          <Modal.Header closeButton={true} aria-label="Cancelar">
            <Modal.Title>Error</Modal.Title>
          </Modal.Header>
          <Modal.Body>{ modalBody }</Modal.Body>
          <Modal.Footer>
            <button className="btn btn-primary" onClick={this.hideModal}>Dismiss</button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

Error.propTypes = {
  errors: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object,
  ]),
  onDismissErrors: PropTypes.func.isRequired,
};

export default Error;
