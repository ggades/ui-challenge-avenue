import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import ErrorContainer from './../containers/ErrorContainer';
import LoadingContainer from './../containers/LoadingContainer';

class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      form: {
        domain: '',
      },
      errors: null,
      defaultZoom: 11,
      latitude: '',
      longitude: '',
    };

    // Bind
    this.validateForm = this.validateForm.bind(this);
    this.locateDomain = this.locateDomain.bind(this);
    this.onFormChange = this.onFormChange.bind(this);
    this.getMyLocation = this.getMyLocation.bind(this);
    this.resetMyLocation = this.resetMyLocation.bind(this);
    this.setMap = this.setMap.bind(this);
  }

  componentDidMount() {
    this.props.stopLoading();
  }

  getMyLocation() {
    const self = this;
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        self.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
        }, () => {
          const geololocation = {
            lat: this.state.latitude,
            lng: this.state.longitude,
          };
          this.setMap(geololocation);
        });
      });
    }
  }

  resetMyLocation() {
    this.setState({
      latitude: this.props.geolocationData.latitude ? this.props.geolocationData.latitude : 0,
      longitude: this.props.geolocationData.longitude ? this.props.geolocationData.longitude : 0,
    }, () => {
      const geololocation = {
        lat: this.state.latitude,
        lng: this.state.longitude,
      };
      this.setMap(geololocation);
    });
  }

  componentWillReceiveProps(props) {
    let geololocation = {
      lat: 0,
      lng: 0,
    };
    if (props.geolocationData.latitude) {
      geololocation = {
        lat: props.geolocationData.latitude,
        lng: props.geolocationData.longitude,
      };
    } else {
      geololocation = {
        lat: this.state.latitude,
        lng: this.state.longitude,
      };
    }

    if (geololocation.lat !== '') {
      this.setMap(geololocation);
    }
  }

  setMap(geololocation) {
    /* eslint-disable */
    const newMap = new google.maps.Map(document.getElementById('map'), {
      center: geololocation,
      zoom: this.state.defaultZoom,
    });

    const marker = new google.maps.Marker({
      position: geololocation,
      map: newMap,
      title: 'Location found!',
    });
    /* eslint-enable */
  }

  onFormChange(e) {
    const newForm = _.assign({}, this.state.form);
    newForm[e.target.name] = e.target.value;

    const newState = _.assign({}, this.state, { form: newForm });
    this.setState(newState);
  }

  validateForm() {
    const domain = this.state.form.domain;
    let condition;
    let message;

    if (/^((?:(?:(?:\w[\.\-\+]?)*)\w)+)((?:(?:(?:\w[\.\-\+]?){0,62})\w)+)\.(\w{2,6})$/.test(domain)) { // eslint-disable-line
      condition = true;
      message = '';
    } else {
      condition = false;
      message = 'Invalid domain';
    }

    return {
      domain: {
        valid: condition,
        message,
      },
    };
  }

  locateDomain(e) {
    e.preventDefault();
    let condition = true;
    const errors = this.validateForm();

    if (!errors.domain.valid) {
      this.setState({ errors });
      condition = false;
    } else {
      this.setState({ errors: null });
      this.props.getGeoLocation(this.state.form.domain);
    }

    return condition;
  }

  render() {
    return (
      <div className="container-fluid">
        <h1>GeoLocation</h1>
        <form className="form-inline" onSubmit={this.locateDomain}>
          <div className="form-group">
            <input
              name="domain"
              className="form-control"
              placeholder="Domain"
              value={this.state.form.domain}
              onChange={this.onFormChange}
            />
            <button type="submit" className="btn btn-primary">Locate</button>
            <div className="error">
              {(this.state.errors && !this.state.errors.domain.valid) ? this.state.errors.domain.message : ''}
            </div>
          </div>
        </form>
        <div className="form-group btn-group text-center clearfix">
          <button
            className="btn btn-sm btn-default"
            onClick={this.getMyLocation}>My location
          </button>
          <button
            className="btn btn-sm btn-default"
            onClick={this.resetMyLocation}>Reset my location
          </button>
        </div>
        <div
          id="map"
          className={this.props.geolocationData.latitude || this.state.latitude !== '' ? 'map' : 'map hidden'}>
        </div>

        <LoadingContainer />
        <ErrorContainer />
      </div>
    );
  }
}

Home.propTypes = {
  geolocationData: PropTypes.object.isRequired,
  getGeoLocation: PropTypes.func.isRequired,
  stopLoading: PropTypes.func.isRequired,
  fetched: PropTypes.bool.isRequired,
};
export default Home;
