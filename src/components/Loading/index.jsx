import React, { PropTypes } from 'react'; // eslint-disable-line
import './style.less';

const Loading = ({ isLoading }) => {
  return (
    <div className={ isLoading ? 'loading-container' : 'loading-container loader-hidden' }>
      <div className="spinner">
        <div className="rect1"></div>
        <div className="rect2"></div>
        <div className="rect3"></div>
        <div className="rect4"></div>
        <div className="rect5"></div>
      </div>
    </div>
  );
};

Loading.propTypes = {
  isLoading: PropTypes.bool.isRequired,
};

export default Loading;
