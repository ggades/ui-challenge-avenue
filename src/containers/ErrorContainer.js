import { connect } from 'react-redux';
import Error from '../components/Error/index.jsx';
import { clearApiError, stopLoading } from '../actions';


function mapStateToProps(state) {
  return {
    errors: state.errors,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onDismissErrors: () => {
      dispatch(stopLoading());
      dispatch(clearApiError());
    },
  };
}

const ErrorContainer = connect(mapStateToProps, mapDispatchToProps)(Error);

export default ErrorContainer;
