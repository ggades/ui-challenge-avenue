import { connect } from 'react-redux';
import Home from '../components/Home.jsx';
import {
  startLoading,
  stopLoading,
  getGeoLocation,
} from '../actions';

function mapStateToProps(state) {
  // console.log(state.errors);
  return {
    geolocationData: state.home.geolocationData,
    fetched: state.home.fetched,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getGeoLocation: (domain) => {
      dispatch(getGeoLocation(domain));
    },
    startLoading: () => {
      dispatch(startLoading());
    },
    stopLoading: () => {
      dispatch(stopLoading());
    },
  };
}

const HomeContainer = connect(mapStateToProps, mapDispatchToProps)(Home);

export default HomeContainer;
