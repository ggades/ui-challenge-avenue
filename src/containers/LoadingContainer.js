import { connect } from 'react-redux';
import Loading from '../components/Loading/index.jsx';

function mapStateToProps(state) {
  return {
    isLoading: state.loading,
  };
}

const LoadingContainer = connect(mapStateToProps, null)(Loading);

export default LoadingContainer;
