import { combineReducers } from 'redux';
import errors from './errors';
import loading from './loading';
import home from './home';

const rootReducer = combineReducers({
  errors,
  loading,
  home,
});

export default rootReducer;
