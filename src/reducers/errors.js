import {
  RECEIVE_API_ERROR,
  CLEAR_API_ERROR,
} from '../actions';

export default function errors(state = [], action) {
  switch (action.type) {
    case RECEIVE_API_ERROR:
      return [action.errorMessage];
    case CLEAR_API_ERROR:
      return [];
    default:
      return state;
  }
}
