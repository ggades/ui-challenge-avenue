import _ from 'lodash';
import {
  SET_DATA,
} from '../actions';

// Initial state of the reducer
const defaultState = {
  geolocationData: {},
  fetched: false,
};

export default function home(state = defaultState, action) {
  switch (action.type) {
    case SET_DATA:
      return _.assign({}, state, {
        geolocationData: action.data,
        fetched: true,
      });
    default:
      return state;
  }
}
