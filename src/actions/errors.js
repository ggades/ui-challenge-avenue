export const RECEIVE_API_ERROR = 'RECEIVE_API_ERROR';
export function receiveApiError(err) {
  let errorMessage = err;
  if (typeof err.message === 'string') {
    errorMessage = [err.message];
  } if (typeof err === 'string') {
    errorMessage = [err];
  }

  return {
    type: RECEIVE_API_ERROR,
    errorMessage,
  };
}

export const CLEAR_API_ERROR = 'CLEAR_API_ERROR';
export function clearApiError() {
  return {
    type: CLEAR_API_ERROR,
  };
}
