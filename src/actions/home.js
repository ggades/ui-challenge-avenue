import axios from 'axios';
import config from '../config';
import { receiveApiError, startLoading, stopLoading } from './index';

export const SET_DATA = 'SET_DATA';
export function receiveData(data) {
  return {
    type: SET_DATA,
    data,
  };
}

/**
 * Get geolocation
 */
export function getGeoLocation(domain) {
  return (dispatch) => {
    dispatch(startLoading());

    axios.get(`${config.endpointApi}/${domain}`).then((response) => {
      dispatch(receiveData(response.data));
      dispatch(stopLoading());
    }).catch((error) => {
      console.log(error); // eslint-disable-line
      dispatch(receiveApiError(error.response));
      dispatch(stopLoading());
    });
  };
}
