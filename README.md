# GeoLocation  web-app
This project uses **ReactJS** Facebok lib with **webpack** module bundler.

Install dependencies:


`npm i`



Compile application & run local server:


`npm run dev`



Compile application to production:


`npm run build`


_Production build will be stored in **/release** folder._



Usefull links:


* [ReactJS docs](https://facebook.github.io/react/)


* [Webpack](https://webpack.github.io/)


* [ESLint docs](http://eslint.org/)


* [Babel transpiler](https://babeljs.io/)
