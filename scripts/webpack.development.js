const webpack = require('webpack');

const config = {
  devServer: {
    historyApiFallback: true,
    stats: 'errors-only',
    inline: true,
    disableHostCheck: true,
  },
  plugins: [
    new webpack.NamedModulesPlugin(),
  ],
  module: {
    rules: [
      {
        test: /\.(less|css)$/,
        use: [{
          loader: 'style-loader',
        }, {
          loader: 'css-loader',
        }, {
          loader: 'less-loader',
        }],
      },
    ],
  },
};

module.exports = config;
